from pycocotools.coco import COCO
from pycocoevalcap.eval import COCOEvalCap
# import matplotlib.pyplot as plt
# import skimage.io as io
# import pylab
# pylab.rcParams['figure.figsize'] = (10.0, 8.0)
import os
import argparse
import json
from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.3f')


parser = argparse.ArgumentParser()
parser.add_argument("--groundtruth", type=str, help="The ground truth captions.")
parser.add_argument("--generated", type=str, help="The generated captions.")
args = parser.parse_args()


# set up file names and pathes
"""
dataDir='.'
dataType='val2014'
algName = '10000'
annFile='%s/annotations/captions_%s.json'%(dataDir,dataType)
subtypes=['results', 'evalImgs', 'eval']
[resFile, evalImgsFile, evalFile]= \
['%s/results/captions_%s_%s_%s.json'%(dataDir,dataType,algName,subtype) for subtype in subtypes]
"""
dataType='msrvtt'
ckptStep= '200000'
startId='9000'
endId='10000'
sample='temporal'
# startId='0000'
# endId='8000'
# sample='boundary'
annFile='annotations/test_videodatainfo_2016_coco_evaluation.json'
# resFile = "results/captions_{}_{}_{}_{}_{}_{}.json".format(dataType, ckptStep, startId, endId, sample, "results")
resFile = "results/generated_caption.json"
"""
evalImgsFile = os.path.join("results",
                "captions_{}_{}_{}_{}_{}_{}.json".format(dataType, algName, startId, endId, sample, "trainMsrvtt"))
evalFile = os.path.join(dataDir, "results",
            "captions_{}_{}_{}_{}_{}_{}.json".format(dataType, algName, startId, endId, sample, "train"))
"""

# create coco object and cocoRes object
coco = COCO(args.groundtruth)
cocoRes = coco.loadRes(args.generated)

# create cocoEval object by taking coco and cocoRes
cocoEval = COCOEvalCap(coco, cocoRes)

# evaluate on a subset of images by setting
# cocoEval.params['image_id'] = cocoRes.getImgIds()
# please remove this line when evaluating the full validation set
# cocoEval.params['image_id'] = cocoRes.getImgIds()
# pdb.set_trace()

scores = cocoEval.evaluate()
print scores
