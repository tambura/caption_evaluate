Evaluate the NLG model such as image caption.

## Requirements

java 1.8.0  
python 2.7

## Convert Format

1. If you are using MS-COCO dataset, please refer to [coco-caption](https://github.com/tylin/coco-caption)
1. If you are using other datasets, convert the reference in ground truth to match the format of COCO: `python convert_meta_to_coco.py`.
This file converts MSRVTT reference to have the same format as COCO.
1. `python evaluate.py --ground-truth reference.json --generated generated.json` to calculate the scores including BLEU, METEOR, ROUGER and CIDEr.
1. The scores will be returned as a python dict for further analysis.

## Acknowledgements

[Evaluation code for MS-COCO caption task](https://github.com/tylin/coco-caption)
