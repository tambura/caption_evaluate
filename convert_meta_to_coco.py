import json
from pprint import pprint


LICENSE = [{u'url': u'http://creativecommons.org/licenses/by-nc-sa/2.0/', u'id': 1, u'name': u'Attribution-NonCommercial-ShareAlike License'}, {u'url': u'http://creativecommons.org/licenses/by-nc/2.0/', u'id': 2, u'name': u'Attribution-NonCommercial License'}, {u'url': u'http://creativecommons.org/licenses/by-nc-nd/2.0/', u'id': 3, u'name': u'Attribution-NonCommercial-NoDerivs License'}, {u'url': u'http://creativecommons.org/licenses/by/2.0/', u'id': 4, u'name': u'Attribution License'}, {u'url': u'http://creativecommons.org/licenses/by-sa/2.0/', u'id': 5, u'name': u'Attribution-ShareAlike License'}, {u'url': u'http://creativecommons.org/licenses/by-nd/2.0/', u'id': 6, u'name': u'Attribution-NoDerivs License'}, {u'url': u'http://flickr.com/commons/usage/', u'id': 7, u'name': u'No known copyright restrictions'}, {u'url': u'http://www.usa.gov/copyright.shtml', u'id': 8, u'name': u'United States Government Work'}]


def detect_non_ascii(caption):
    try:
        caption.decode('utf-8')
    except UnicodeEncodeError as e:
        print e


def delete_non_ascii(caption):
    caption = caption.replace(u'\xe9', 'e')
    caption = caption.replace(u'\u2019', "'")
    caption = caption.replace(u'\u0432', 'b')
    return caption

# Convert the captions in msrvtt to have the same structure as mscoco annotations.
# In order to use the evaluate code in repository: github.com/tylin/coco-caption
def convert_msrvtt_to_coco_format(src_file):
    with open(src_file) as f:
        anno = json.load(f)

    to_save = {"images":[],
               "annotations":[]}

    for video in anno["videos"]:
        to_save["images"].append({"id": video["id"]})

    for sentence in anno["sentences"]:
        to_save["annotations"].append(
            {"image_id": int(sentence["video_id"][5:]),
            "caption": delete_non_ascii(sentence["caption"]),
            "id": sentence["sen_id"]
            })

    to_save["type"] = "captions"
    to_save["info"] = anno["info"]
    to_save["licenses"] = LICENSE

    return to_save  # Annotations for the whole dataset in coco format.


def split(src_file, train_amount, val_amount, test_amount, train_datainfo, val_datainfo, test_datainfo):
  to_save_train = dict()
  to_save_val = dict()
  to_save_test = dict()
  whole = convert_msrvtt_to_coco_format(src_file)
  
  to_save_train["type"] = whole["type"]
  to_save_train["info"] = whole["info"]
  to_save_train["licenses"] = whole["licenses"]
  to_save_train["images"] = list()
  to_save_train["annotations"] = list()

  to_save_val["type"] = whole["type"]
  to_save_val["info"] = whole["info"]
  to_save_val["licenses"] = whole["licenses"]
  to_save_val["images"] = list()
  to_save_val["annotations"] = list()

  to_save_test["type"] = whole["type"]
  to_save_test["info"] = whole["info"]
  to_save_test["licenses"] = whole["licenses"]
  to_save_test["images"] = list()
  to_save_test["annotations"] = list()

  for image in whole["images"]:
    if image["id"] < train_amount:
      to_save_train["images"].append(image)
    elif image["id"] < train_amount + val_amount:
      to_save_val["images"].append(image)
    elif image["id"] < train_amount + val_amount + test_amount:
      to_save_test["images"].append(image)

  for annotation in whole["annotations"]:
    if annotation["image_id"] < train_amount:
      to_save_train["annotations"].append(annotation)
    elif annotation["image_id"] < train_amount + val_amount:
      to_save_val["annotations"].append(annotation)
    elif annotation["image_id"] < train_amount + val_amount + test_amount:
      to_save_test["annotations"].append(annotation)

  with open(train_datainfo, 'w') as f:
    json.dump(to_save_train, f)
  with open(val_datainfo, 'w') as f:
    json.dump(to_save_val, f)
  with open(test_datainfo, 'w') as f:
    json.dump(to_save_test, f)


def check():
  with open("captions_msrvtt_gt_test.json") as f:
    c = json.load(f)
    print c["type"]
    print c["info"]
    print c["images"][0]
    print c["licenses"]
    print c["annotations"][0]


if __name__ == "__main__":
  split("videodatainfo_2017.json",
        8000,
        500,
        1500,
        "videodata_info_0000_8000_coco_eval.json",
        "videodata_info_8000_8500_coco_eval.json",
        "videodata_info_8500_10000_coco_eval.json")
  # check()
